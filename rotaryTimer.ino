#include <Timer.h>
#include <LiquidCrystal.h>
#include <EEPROM.h>
#include <avr/eeprom.h>

enum Context {
  mainMenu,
  run,
  chooseVar,
  setTimeOn,
  setTimeOff, 
};

// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

Context context;

Timer t;
boolean outState = false;
int timeLeft;

int currentEventId;

const int buttonPin = 6;    // the number of the pushbutton pin
const int outputPin = 9;

int buttonState;             
int lastButtonState = LOW;

long lastDebounceTime = 0;  // the last time the output pin was toggled
long debounceDelay = 50;    // the debounce time; increase if the output flickers

int timeOn = 5;
int timeOff = 5;

int encoder0PinA = 8;
 int encoder0PinB = 7;
 int encoder0Pos = 0;
 int encoder0PinALast = LOW;
 int n = LOW;
 
int addressOn = 0;
int addressOff = 100;

int contIncr = 0;

byte arrow[8] = {
  B00000,
  B00100,
  B00110,
  B00111,
  B00110,
  B00100,
  B00000,
};

void setup() {
  lcd.begin(16, 2);
  
  initMainMenu();
  lcd.createChar(0, arrow);
  
  eeprom_read_block((void*)&timeOn, (void*)addressOn, sizeof(int));
  eeprom_read_block((void*)&timeOff, (void*)addressOff, sizeof(int));
  
  //timeOn = EEPROM.read(addressOn);
  //timeOff = EEPROM.read(addressOff);
  
  pinMode(buttonPin, INPUT);
  pinMode (encoder0PinA,INPUT);
  pinMode (encoder0PinB,INPUT);
  
  pinMode(outputPin, OUTPUT);
  
  Serial.begin (9600);

}

void initMainMenu() {
  contIncr = 0;
  context = mainMenu;
  
  lcd.clear();
  lcd.print("Vyber akci:");
  lcd.setCursor(0,1);
  lcd.write(byte(0));
  lcd.print("Spust      Zmen");
}

void rotaryMainMenu() {
  contIncr = !contIncr;
 
  if (contIncr) {
    lcd.setCursor(0,1);
    lcd.print(" ");
    lcd.setCursor(11,1);
    lcd.write(byte(0));
  } else {
    lcd.setCursor(0,1);
    lcd.write(byte(0));
    lcd.setCursor(11,1);
    lcd.print(" ");
  }
}

void buttonMainMenu() {
  if (contIncr) {
    initChooseVar();
  } else {
    initRun();
  } 
}

void initChooseVar() {
  contIncr = 0;
  context = chooseVar;
  
  lcd.clear();
  lcd.print("Zmenit dobu:");
  lcd.setCursor(0,1);
  lcd.write(byte(0));
  lcd.print("ON   OFF   Zpet");
}

void rotaryChooseVar(boolean dir) {
  if (dir) {
    contIncr++;
  } else { 
    contIncr--;
  }
  
  if (contIncr < 0) contIncr += 3;
  
  contIncr = contIncr % 3;
  
  lcd.setCursor(0,1);
  lcd.print(" ");
  lcd.setCursor(5,1);
  lcd.print(" ");
  lcd.setCursor(11,1);
  lcd.print(" ");
  
  switch (contIncr) {
    case 0:
     lcd.setCursor(0,1);
     break;
    case 1:
     lcd.setCursor(5,1);
     break;
    case 2:
     lcd.setCursor(11,1);
     break;
  }
  lcd.write(byte(0));
}

void buttonChooseVar() {
  switch (contIncr) {
    case 0:
     initSetTimeOn();
     break;
    case 1:
     initSetTimeOff();
     break;
    case 2:
     initMainMenu(); 
  }
}

void initSetTimeOn() {
  context = setTimeOn;
  
  lcd.clear();
  lcd.print("Nastavit cas ON:");
  lcd.setCursor(0,1);
  lcd.print(timeOn);
  lcd.print("s");
}

void rotarySetTimeOn(boolean dir) {
  if (dir) {
   timeOn++;
  } else {
   timeOn--; 
   if (timeOn < 0) timeOn = 0;
  }
  lcd.setCursor(0,1);
  lcd.print("        ");
  lcd.setCursor(0,1);
  lcd.print(timeOn);
  lcd.print("s");
}

void buttonSetTimeOn() {
   eeprom_write_block((const void*)&timeOn, (void*)addressOn, sizeof(int));
   
   initChooseVar();
}

void initSetTimeOff() {
  context = setTimeOff;
  
  lcd.clear();
  lcd.print("Nastavit cas OFF:");
  lcd.setCursor(0,1);
  lcd.print(timeOff);
  lcd.print("s");
}

void rotarySetTimeOff(boolean dir) {
  if (dir) {
   timeOff++;
  } else {
   timeOff--; 
   if (timeOff < 0) timeOff = 0;
  }
  lcd.setCursor(0,1);
  lcd.print("        ");
  lcd.setCursor(0,1);
  lcd.print(timeOff);
  lcd.print("s");
}

void buttonSetTimeOff() {

   eeprom_write_block((const void*)&timeOff, (void*)addressOff, sizeof(int));
   
   initChooseVar();
}

void initRun() {
  context = run;
  outState = false;
  timeLeft = 0;
  
  lcd.clear();
  lcd.print("Pripravit!");
    
  currentEventId = t.every(1000, refreshTime);
}

void refreshTime() {
 
  if (timeLeft == 0) {
    outState = !outState; 
    
    timeLeft = (outState?timeOn:timeOff) - 1;
    
    lcd.clear();
    lcd.print("Zbyva ");
    lcd.print(timeLeft);
    lcd.print("s");
    lcd.setCursor(0,1);
    lcd.print("Pristi ");
    lcd.print(!outState?"ON ":"OFF ");
    lcd.print(!outState?timeOn:timeOff);
    lcd.print("s");
    
    digitalWrite(outputPin, outState?HIGH:LOW);
  } else {
    timeLeft--;
    
    lcd.setCursor(6,0);
    lcd.print("      ");
    lcd.setCursor(6,0);
    lcd.print(timeLeft);
    lcd.print("s");
  }
}

void buttonRun() {
 t.stop(currentEventId);
 outState = false;
 digitalWrite(outputPin, LOW);
 initMainMenu(); 
}

void catchRotary() {
   n = digitalRead(encoder0PinA);
   if ((encoder0PinALast == LOW) && (n == HIGH)) {
     boolean rotaryDir = digitalRead(encoder0PinB) == LOW;
     
     switch (context) {
       case mainMenu:
        rotaryMainMenu();
        break; 
       case chooseVar:
        rotaryChooseVar(rotaryDir);
        break;
       case setTimeOn:
        rotarySetTimeOn(rotaryDir);
        break;
       case setTimeOff:
        rotarySetTimeOff(rotaryDir);
        break;
     }
   } 
   encoder0PinALast = n;
}

void catchButton() {
  int reading = digitalRead(buttonPin);
      
  if (reading != lastButtonState) {
    lastDebounceTime = millis();
  } 
  
  if ((millis() - lastDebounceTime) > debounceDelay) {
   
    if (reading != buttonState) {
      buttonState = reading;
      
      if (buttonState == HIGH) {        
        switch (context) {
          case mainMenu:
           buttonMainMenu();
           break; 
          case chooseVar:
           buttonChooseVar();
           break;
          case setTimeOn:
           buttonSetTimeOn();
           break;
          case setTimeOff:
           buttonSetTimeOff();
           break; 
          case run:
           buttonRun();
           break;
        }
      }
    }
  }
  
  lastButtonState = reading;
}

void loop() {
  
  catchRotary();
  catchButton();
  t.update();
}
